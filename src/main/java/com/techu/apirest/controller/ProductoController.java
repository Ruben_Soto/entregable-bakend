package com.techu.apirest.controller;

import com.techu.apirest.ProductoPrecio;
import com.techu.apirest.model.ProductoModel;
import com.techu.apirest.service.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class ProductoController {

    @Autowired
    ProductoService productoService;
    ProductoPrecio productoPrecio;

    //Trae todos los productos de la colección
    @GetMapping("/productos")
    public List<ProductoModel> getProductos(){

        return productoService.findAll();
    }

    //Trae un producto por ID
    @GetMapping("/productos/{id}")
    public Optional<ProductoModel> getProductoId(@PathVariable String id){

        return productoService.findById(id);
    }


    @PostMapping("/productos")
    public ProductoModel postProductos(@RequestBody ProductoModel newProducto){
        productoService.save(newProducto);
        return newProducto;
    }

    //Put tb se puede hacersin id tal como viene en el codelab y con void
    //Save extiende de mongodb y permite no solo añadir sino tb actualizar cuando el id existe. De ahi que el codigo sea igual que el post
    @PutMapping("/productos/{id}")
    public ProductoModel putProductos(@RequestBody ProductoModel productoToUpdate, @PathVariable String id){
        //if (getProductoId(id).isPresent()){
        if (productoService.findById(id).isPresent()){
        productoService.save(productoToUpdate);
        return productoToUpdate;
        }
        return new ProductoModel("null", "null", 0.0);
    }

    //put sin id
    @PutMapping("/productos")
    public void putProductos(@RequestBody ProductoModel productoToUpdate){
        productoService.save(productoToUpdate);
    }

    //boolean porque en service tb
    @DeleteMapping("/productos")
    public boolean deleteProductos(@RequestBody ProductoModel productoToDelete){
       return productoService.delete(productoToDelete);
    }

    @DeleteMapping("/productos/{id}")
    public boolean deleteProductos(@RequestBody ProductoModel productoToDelete, @PathVariable String id){
        //if (getProductoId(id).isPresent()){
        if (productoService.findById(id).isPresent()){
            productoService.delete(productoToDelete);
            //se puede poner el return directamente arriba
            return true;
        }
        return false;
    }

    //Otra opcion mas sencilla de patch
   @PatchMapping("/productos/{id}")
    public ProductoModel patchProductos(@RequestBody ProductoModel productoToPatch, @PathVariable String id){

       productoPrecio.setPrecio(productoToPatch.getPrecio());

       productoService.save(productoToPatch);
       return productoToPatch;

    }

    @PatchMapping("/productos/{precio}")
    public boolean patchProductos(@RequestBody ProductoModel productoToPatch, @PathVariable Double precio){
        if (productoService.findById(productoToPatch.getId()).isPresent()){
            productoToPatch.setPrecio(precio);
            productoService.save(productoToPatch);
            return true;
        }else
            return false;

    }

    /*
    @PatchMapping("/productos/{id}")
    public ProductoModel patchProductos(@RequestBody Map<String, Object> update, @PathVariable String id){
        ProductoModel productopatchear;
        Optional<ProductoModel> oproducto=productoService.findById(id);
        if (oproducto.isPresent()){
            productopatchear = oproducto.get();
        }
        else
            return new ProductoModel("0", "id no existe", 0.0);
        if (update.containsKey("precio"))
            productopatchear.setPrecio((Double) update.get("precio"));
        if (update.containsKey("descripcion"))
            productopatchear.setDescripcion((String) update.get("descripcion"));

       productoService.save(productopatchear);
       return productopatchear;

    }*/

}
